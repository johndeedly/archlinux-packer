packer {
  required_plugins {
    virtualbox = {
      source  = "github.com/hashicorp/virtualbox"
      version = "~> 1"
    }
    ansible = {
      source  = "github.com/hashicorp/ansible"
      version = "~> 1"
    }
  }
}


variable "yearmonthday" {
  type = string
}

variable "sound_driver" {
  type = string
}

variable "accel_graphics" {
  type = string
}

variable "encryption" {
  type = bool
  default = false
}

variable "dualboot" {
  type = bool
  default = false
}

variable "headless" {
  type = bool
  default = true
}


source "virtualbox-iso" "bootstrap" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 524288
  format                   = "ovf"
  guest_additions_mode     = "disable"
  guest_os_type            = "ArchLinux_64"
  hard_drive_discard       = true
  hard_drive_interface     = "sata"
  hard_drive_nonrotational = true
  headless                 = var.headless
  iso_checksum             = "none"
  iso_interface            = "sata"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/bootstrap"
  output_filename          = "../archlinux-bootstrap-${var.yearmonthday}-x86_64"
  ssh_password             = "toor"
  ssh_timeout              = "10m"
  ssh_username             = "root"
  vboxmanage               = [["modifyvm", "{{ .Name }}", "--chipset", "ich9", "--firmware", "efi", "--cpus", "4", "--memory", "3072", "--audio-driver", "${var.sound_driver}", "--audio-out", "on", "--audio-enabled", "on", "--usb", "on", "--usb-xhci", "on", "--clipboard", "hosttoguest", "--draganddrop", "hosttoguest", "--graphicscontroller", "vmsvga", "--acpi", "on", "--ioapic", "on", "--apic", "on", "--accelerate3d", "${var.accel_graphics}", "--accelerate2dvideo", "on", "--vram", "128", "--pae", "on", "--nested-hw-virt", "on", "--paravirtprovider", "kvm", "--hpet", "on", "--hwvirtex", "on", "--largepages", "on", "--vtxvpid", "on", "--vtxux", "on", "--biosbootmenu", "messageandmenu", "--rtcuseutc", "on", "--nictype1", "virtio", "--macaddress1", "auto", "--nic2", "intnet", "--nictype2", "virtio", "--cableconnected2", "on", "--intnet2", "locally", "--macaddress2", "auto", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vboxmanage_post          = [["modifyvm", "{{ .Name }}", "--memory", "1536", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "disk", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vm_name                  = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-iso" "localmirror" {
  shutdown_command     = "/sbin/poweroff"
  cd_files             = ["CIDATA/*"]
  cd_label             = "CIDATA"
  format               = "ovf"
  guest_additions_mode = "disable"
  headless             = var.headless
  output_directory     = "output/localmirror"
  output_filename      = "../archlinux-localmirror-${var.yearmonthday}-x86_64"
  source_path          = "output/archlinux-bootstrap-${var.yearmonthday}-x86_64.ovf"
  ssh_password         = "toor"
  ssh_timeout          = "10m"
  ssh_username         = "root"
  vboxmanage           = [
    ["modifyvm", "{{ .Name }}", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"],
    ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--port", "1", "--device", "0", "--type", "dvddrive", "--medium", "archlinux-${var.yearmonthday}-x86_64.iso"]
  ]
  vboxmanage_post      = [
    ["modifyvm", "{{ .Name }}", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "disk", "--boot2", "none", "--boot3", "none", "--boot4", "none"],
    ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--port", "1", "--device", "0", "--type", "dvddrive", "--medium", "none"]
  ]
  vm_name              = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-iso" "database" {
  shutdown_command     = "/sbin/poweroff"
  cd_files             = ["CIDATA/*"]
  cd_label             = "CIDATA"
  format               = "ovf"
  guest_additions_mode = "disable"
  headless             = var.headless
  output_directory     = "output/database"
  output_filename      = "../archlinux-database-${var.yearmonthday}-x86_64"
  source_path          = "output/archlinux-bootstrap-${var.yearmonthday}-x86_64.ovf"
  ssh_password         = "toor"
  ssh_timeout          = "10m"
  ssh_username         = "root"
  vboxmanage           = [
    ["modifyvm", "{{ .Name }}", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"],
    ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--port", "1", "--device", "0", "--type", "dvddrive", "--medium", "archlinux-${var.yearmonthday}-x86_64.iso"]
  ]
  vboxmanage_post      = [
    ["modifyvm", "{{ .Name }}", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "disk", "--boot2", "none", "--boot3", "none", "--boot4", "none"],
    ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--port", "1", "--device", "0", "--type", "dvddrive", "--medium", "none"]
  ]
  vm_name              = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-iso" "router" {
  shutdown_command     = "/sbin/poweroff"
  cd_files             = ["CIDATA/*"]
  cd_label             = "CIDATA"
  format               = "ovf"
  guest_additions_mode = "disable"
  headless             = var.headless
  output_directory     = "output/router"
  output_filename      = "../archlinux-router-${var.yearmonthday}-x86_64"
  source_path          = "output/archlinux-bootstrap-${var.yearmonthday}-x86_64.ovf"
  ssh_password         = "toor"
  ssh_timeout          = "10m"
  ssh_username         = "root"
  vboxmanage           = [
    ["modifyvm", "{{ .Name }}", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"],
    ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--port", "1", "--device", "0", "--type", "dvddrive", "--medium", "archlinux-${var.yearmonthday}-x86_64.iso"]
  ]
  vboxmanage_post      = [
    ["modifyvm", "{{ .Name }}", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "disk", "--boot2", "none", "--boot3", "none", "--boot4", "none"],
    ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--port", "1", "--device", "0", "--type", "dvddrive", "--medium", "none"]
  ]
  vm_name              = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-iso" "graphical" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 524288
  format                   = "ovf"
  guest_additions_mode     = "disable"
  guest_os_type            = "ArchLinux_64"
  hard_drive_discard       = true
  hard_drive_interface     = "sata"
  hard_drive_nonrotational = true
  headless                 = var.headless
  iso_checksum             = "none"
  iso_interface            = "sata"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/graphical"
  output_filename          = "../archlinux-graphical-${var.yearmonthday}-x86_64"
  ssh_password             = "toor"
  ssh_timeout              = "10m"
  ssh_username             = "root"
  vboxmanage               = [["modifyvm", "{{ .Name }}", "--chipset", "ich9", "--firmware", "efi", "--cpus", "4", "--memory", "3072", "--audio-driver", "${var.sound_driver}", "--audio-out", "on", "--audio-enabled", "on", "--usb", "on", "--usb-xhci", "on", "--clipboard", "hosttoguest", "--draganddrop", "hosttoguest", "--graphicscontroller", "vmsvga", "--acpi", "on", "--ioapic", "on", "--apic", "on", "--accelerate3d", "${var.accel_graphics}", "--accelerate2dvideo", "on", "--vram", "128", "--pae", "on", "--nested-hw-virt", "on", "--paravirtprovider", "kvm", "--hpet", "on", "--hwvirtex", "on", "--largepages", "on", "--vtxvpid", "on", "--vtxux", "on", "--biosbootmenu", "messageandmenu", "--rtcuseutc", "on", "--nictype1", "virtio", "--macaddress1", "auto", "--nic2", "intnet", "--nictype2", "virtio", "--cableconnected2", "on", "--intnet2", "locally", "--macaddress2", "auto", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vboxmanage_post          = [["modifyvm", "{{ .Name }}", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "disk", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vm_name                  = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-iso" "debugger" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 524288
  format                   = "ovf"
  guest_additions_mode     = "disable"
  guest_os_type            = "ArchLinux_64"
  hard_drive_discard       = true
  hard_drive_interface     = "sata"
  hard_drive_nonrotational = true
  headless                 = var.headless
  iso_checksum             = "none"
  iso_interface            = "sata"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/debugger"
  output_filename          = "../archlinux-debugger-${var.yearmonthday}-x86_64"
  ssh_password             = "toor"
  ssh_timeout              = "10m"
  ssh_username             = "root"
  vboxmanage               = [["modifyvm", "{{ .Name }}", "--chipset", "ich9", "--firmware", "efi", "--cpus", "4", "--memory", "3072", "--audio-driver", "${var.sound_driver}", "--audio-out", "on", "--audio-enabled", "on", "--usb", "on", "--usb-xhci", "on", "--clipboard", "hosttoguest", "--draganddrop", "hosttoguest", "--graphicscontroller", "vmsvga", "--acpi", "on", "--ioapic", "on", "--apic", "on", "--accelerate3d", "${var.accel_graphics}", "--accelerate2dvideo", "on", "--vram", "128", "--pae", "on", "--nested-hw-virt", "on", "--paravirtprovider", "kvm", "--hpet", "on", "--hwvirtex", "on", "--largepages", "on", "--vtxvpid", "on", "--vtxux", "on", "--biosbootmenu", "messageandmenu", "--rtcuseutc", "on", "--nictype1", "virtio", "--macaddress1", "auto", "--nic2", "intnet", "--nictype2", "virtio", "--cableconnected2", "on", "--intnet2", "locally", "--macaddress2", "auto", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vboxmanage_post          = [["modifyvm", "{{ .Name }}", "--macaddress1", "auto", "--macaddress2", "auto", "--boot1", "disk", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vm_name                  = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-iso" "custom" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 131072
  format                   = "ovf"
  guest_additions_mode     = "disable"
  guest_os_type            = "ArchLinux_64"
  hard_drive_discard       = true
  hard_drive_interface     = "sata"
  hard_drive_nonrotational = true
  headless                 = var.headless
  iso_checksum             = "none"
  iso_interface            = "sata"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/custom"
  skip_export              = true
  ssh_password             = "toor"
  ssh_timeout              = "10m"
  ssh_username             = "root"
  vboxmanage               = [["modifyvm", "{{ .Name }}", "--chipset", "ich9", "--firmware", "efi", "--cpus", "4", "--memory", "3072", "--audio-driver", "${var.sound_driver}", "--audio-out", "on", "--audio-enabled", "on", "--usb", "on", "--usb-xhci", "on", "--clipboard", "hosttoguest", "--draganddrop", "hosttoguest", "--graphicscontroller", "vmsvga", "--acpi", "on", "--ioapic", "on", "--apic", "on", "--accelerate3d", "${var.accel_graphics}", "--accelerate2dvideo", "on", "--vram", "128", "--pae", "on", "--nested-hw-virt", "on", "--paravirtprovider", "kvm", "--hpet", "on", "--hwvirtex", "on", "--largepages", "on", "--vtxvpid", "on", "--vtxux", "on", "--biosbootmenu", "messageandmenu", "--rtcuseutc", "on", "--nictype1", "virtio", "--macaddress1", "auto", "--nic2", "intnet", "--nictype2", "virtio", "--cableconnected2", "on", "--intnet2", "locally", "--macaddress2", "auto", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vm_name                  = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

source "virtualbox-iso" "bootstrap-buildah" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 131072
  format                   = "ovf"
  guest_additions_mode     = "disable"
  guest_os_type            = "ArchLinux_64"
  hard_drive_discard       = true
  hard_drive_interface     = "sata"
  hard_drive_nonrotational = true
  headless                 = var.headless
  iso_checksum             = "none"
  iso_interface            = "sata"
  iso_url                  = "output/archlinux-custom-x86_64.iso"
  output_directory         = "output/bootstrap-buildah"
  skip_export              = true
  ssh_password             = "toor"
  ssh_timeout              = "10m"
  ssh_username             = "root"
  vboxmanage               = [["modifyvm", "{{ .Name }}", "--chipset", "ich9", "--firmware", "efi", "--cpus", "4", "--memory", "3072", "--audio-driver", "${var.sound_driver}", "--audio-out", "on", "--audio-enabled", "on", "--usb", "on", "--usb-xhci", "on", "--clipboard", "hosttoguest", "--draganddrop", "hosttoguest", "--graphicscontroller", "vmsvga", "--acpi", "on", "--ioapic", "on", "--apic", "on", "--accelerate3d", "${var.accel_graphics}", "--accelerate2dvideo", "on", "--vram", "128", "--pae", "on", "--nested-hw-virt", "on", "--paravirtprovider", "kvm", "--hpet", "on", "--hwvirtex", "on", "--largepages", "on", "--vtxvpid", "on", "--vtxux", "on", "--biosbootmenu", "messageandmenu", "--rtcuseutc", "on", "--nictype1", "virtio", "--macaddress1", "auto", "--nic2", "intnet", "--nictype2", "virtio", "--cableconnected2", "on", "--intnet2", "locally", "--macaddress2", "auto", "--boot1", "dvd", "--boot2", "none", "--boot3", "none", "--boot4", "none"]]
  vm_name                  = replace(timestamp(), ":", "꞉") # unicode replacement char for colon
}

build {
  sources = ["source.virtualbox-iso.bootstrap"]

  # prepare ansible in archiso environment
  provisioner "shell" {
    inline = [
      "rm /usr/lib/python3*/EXTERNALLY-MANAGED",
      "python -m ensurepip 2>/dev/null",
      "python -m pip install --upgrade pip setuptools wheel",
      "python -m pip install ansible-core",
      "tee ~/.ansible.cfg <<EOF",
      "[defaults]",
      "interpreter_python=auto_silent",
      "EOF",
      "ansible-galaxy -vvvv collection install community.general"
    ]
    environment_vars = [
      "PIP_ROOT_USER_ACTION=ignore"
    ]
  }

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso.sh",
      "/root/prepare-archiso.sh --device /dev/sda ${var.encryption ? "--encryption atad" : "" } ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/finalize-archiso.sh",
      "/root/finalize-archiso.sh --device /dev/sda ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }
}

build {
  sources = ["source.virtualbox-iso.localmirror"]

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso.sh",
      "/root/prepare-archiso.sh --device /dev/sda ${var.encryption ? "--encryption atad" : "" } ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap -s localmirror"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/finalize-archiso.sh",
      "/root/finalize-archiso.sh --device /dev/sda ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }
}

build {
  sources = ["source.virtualbox-iso.database"]

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso.sh",
      "/root/prepare-archiso.sh --device /dev/sda ${var.encryption ? "--encryption atad" : "" } ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap -s database"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/finalize-archiso.sh",
      "/root/finalize-archiso.sh --device /dev/sda ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }
}

build {
  sources = ["source.virtualbox-iso.router"]

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso.sh",
      "/root/prepare-archiso.sh --device /dev/sda ${var.encryption ? "--encryption atad" : "" } ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap -s router"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/finalize-archiso.sh",
      "/root/finalize-archiso.sh --device /dev/sda ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }
}

build {
  sources = ["source.virtualbox-iso.graphical"]

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso.sh",
      "/root/prepare-archiso.sh --device /dev/sda ${var.encryption ? "--encryption atad" : "" } ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap -s graphical -s cinnamon"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/finalize-archiso.sh",
      "/root/finalize-archiso.sh --device /dev/sda ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }
}

build {
  sources = ["source.virtualbox-iso.debugger"]

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso.sh",
      "/root/prepare-archiso.sh --device /dev/sda ${var.encryption ? "--encryption atad" : "" } ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap -s graphical -s qtile -s debugger"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/finalize-archiso.sh",
      "/root/finalize-archiso.sh --device /dev/sda ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }
}

build {
  sources = ["source.virtualbox-iso.custom"]

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso-custom.sh",
      "/root/prepare-archiso-custom.sh --device /dev/sda --yes"
    ]
  }

  provisioner "file" {
    destination = "output/"
    direction   = "download"
    source      = "/install/root/out/archlinux-custom-x86_64.iso"
  }
}

build {
  sources = ["source.virtualbox-iso.bootstrap-buildah"]

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso-buildah.sh",
      "/root/prepare-archiso-buildah.sh --device /dev/sda --yes"
    ]
  }
  
  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/finalize-archiso-buildah.sh",
      "/root/finalize-archiso-buildah.sh --device /dev/sda --yes"
    ]
  }

  provisioner "file" {
    destination = "output/archlinux-bootstrap.tar.gz"
    direction   = "download"
    source      = "/install/worker.tar.gz"
  }
}
