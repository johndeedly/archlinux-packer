#!/usr/bin/env pwsh
Param(
    [Parameter(Mandatory=$False)]
    [switch]$PxeBoot
)

$WebClient = New-Object System.Net.WebClient
$data = $WebClient.DownloadString("https://archlinux.org/download/")
$match = [regex]::Match($data, 'magnet:.*?dn=archlinux-(.*?)-x86_64.iso')
if ($match.Success) {
	$env:PKR_VAR_yearmonthday = $match.Groups[1].Value
}

$hosturl = "http://ftp.halifax.rwth-aachen.de/archlinux/"
#if ([System.IO.File]::Exists("/etc/pacman.d/localmirror")) {
#	$localmirror = [regex]::Match([System.IO.File]::ReadAllText("/etc/pacman.d/localmirror"), 'https?://.*?/')
#	if ($localmirror.Success) {
#		$hosturl = $localmirror.Value.ToLower()
#	}
#}
$remotepath = "$($hosturl)iso/$($env:PKR_VAR_yearmonthday)/archlinux-$($env:PKR_VAR_yearmonthday)-x86_64.iso"
$localpath = "archlinux-$($env:PKR_VAR_yearmonthday)-x86_64.iso"
Write-Host "<= $($remotepath)"
Write-Host "=> $($localpath)"
if (-Not (Test-Path($localpath))) {
	Invoke-WebRequest -UserAgent "Mozilla/5.0 (X11; Linux x86_64; rv:103.0) Gecko/20100101 Firefox/103.0" -Uri $remotepath -OutFile $localpath
}

$hashsrc = (Get-FileHash $localpath -Algorithm "SHA256").Hash.ToLower()
$remotehash = "http://ftp.halifax.rwth-aachen.de/archlinux/iso/$($env:PKR_VAR_yearmonthday)/sha256sums.txt"
$hash = $WebClient.DownloadString($remotehash)
if ($hash -match '^[a-fA-F0-9]+(?=.*?\.iso)(?!.*?bootstrap)') {
	$hashdst = $matches[0].ToLower()
	if ($hashsrc -ne $hashdst) {
		Write-Host "[!] download is broken"
		break
	} else {
		Write-Host "$($hashsrc)  $($localpath)"
	}
}

if (-Not (Test-Path($localpath))) {
	Write-Host "[!] no iso named $($localpath) in working directory"
	break
}

function Packer-BuildAppliance {
	param([Parameter()][string]$SearchFileName, [Parameter()][string]$Filter, [Parameter()][string]$ArgList)
	$runit = $false
	if ([System.String]::IsNullOrEmpty($SearchFileName)) {
		$runit = $true
	} else {
		$files = [System.IO.Directory]::GetFiles($PWD.ProviderPath + "/output", $SearchFileName, [System.IO.SearchOption]::AllDirectories)	
		if (-Not([System.String]::IsNullOrEmpty($Filter))) {
			$files = [Linq.Enumerable]::Where($files, [Func[string,bool]]{ param($x) $x -match $Filter })
		}
		$file = [Linq.Enumerable]::FirstOrDefault($files)
		Write-Host $file
		if ([System.String]::IsNullOrEmpty($file)) {
			$runit = $true
		}
	}
	if ($runit) {
		if ($IsWindows -or $env:OS) {
			$env:PKR_VAR_sound_driver = "dsound"
			$env:PKR_VAR_accel_graphics = "off"
			$process = Start-Process -PassThru -Wait -NoNewWindow -FilePath "packer.exe" -ArgumentList $ArgList
			return $process.ExitCode
		} else {
			$env:PKR_VAR_sound_driver = "pulse"
			$env:PKR_VAR_accel_graphics = "on"
			$process = Start-Process -PassThru -Wait -FilePath "packer" -ArgumentList $ArgList
			return $process.ExitCode
		}
	}
	return 0
}

New-Item -Path $PWD.ProviderPath -Name "output" -ItemType "directory" -Force | Out-Null
$env:PACKER_LOG=1
if ($IsWindows -or $env:OS) {
  # VBOX
  # $env:PACKER_LOG_PATH="output/bootstrap-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*bootstrap-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -on-error=ask -only=virtualbox-iso.bootstrap archlinux.pkr.hcl") -ne 0) {
  # 	break
  # }
  # $env:PACKER_LOG_PATH="output/localmirror-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*localmirror-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -on-error=ask -only=virtualbox-iso.localmirror archlinux.pkr.hcl") -ne 0) {
  # 	break
  # }
  # $env:PACKER_LOG_PATH="output/database-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*database-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -on-error=ask -only=virtualbox-iso.database archlinux.pkr.hcl") -ne 0) {
  # 	break
  # }
  # $env:PACKER_LOG_PATH="output/router-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*router-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -on-error=ask -only=virtualbox-iso.router archlinux.pkr.hcl") -ne 0) {
  # 	break
  # }
  $env:PACKER_LOG_PATH="output/graphical-packerlog.txt"
  if ((Packer-BuildAppliance -SearchFileName "*graphical-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -on-error=ask -only=virtualbox-iso.graphical archlinux.pkr.hcl") -ne 0) {
  	break
  }
  # $env:PACKER_LOG_PATH="output/debugger-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*debugger-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -on-error=ask -only=virtualbox-iso.debugger archlinux.pkr.hcl") -ne 0) {
  # 	break
  # }
  # $env:PACKER_LOG_PATH="output/custom-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*archlinux-custom-x86_64.iso" -ArgList "build -force -on-error=ask -only=virtualbox-iso.custom archlinux.pkr.hcl") -ne 0) {
  # 	break
  # }
  # $env:PACKER_LOG_PATH="output/bootstrap-buildah-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*archlinux-bootstrap.tar.gz" -ArgList "build -force -on-error=ask -only=virtualbox-iso.bootstrap-buildah archlinux.pkr.hcl") -ne 0) {
  # 	break
  # }
} else {
  # VBOX
  # $env:PACKER_LOG_PATH="output/bootstrap-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*bootstrap-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -on-error=ask -only=virtualbox-iso.bootstrap archlinux.pkr.hcl") -ne 0) {
  # 	break
  # }
  # $env:PACKER_LOG_PATH="output/graphical-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*graphical-$($env:PKR_VAR_yearmonthday)*.ovf" -ArgList "build -force -on-error=ask -only=virtualbox-iso.graphical archlinux.pkr.hcl") -ne 0) {
  # 	break
  # }
  # QEMU
  # $env:PACKER_LOG_PATH="output/bootstrap-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*bootstrap-$($env:PKR_VAR_yearmonthday)*.run.sh" -ArgList "build -on-error=ask -only=qemu.bootstrap archlinux.qemu.pkr.hcl") -ne 0) {
  # 	break
  # }
  # $env:PACKER_LOG_PATH="output/router-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*router-$($env:PKR_VAR_yearmonthday)*.run.sh" -ArgList "build -on-error=ask -only=qemu.router archlinux.qemu.pkr.hcl") -ne 0) {
  # 	break
  # }
  $env:PACKER_LOG_PATH="output/graphical-packerlog.txt"
  if ((Packer-BuildAppliance -SearchFileName "*graphical-$($env:PKR_VAR_yearmonthday)*.run.sh" -ArgList "build -on-error=ask -only=qemu.graphical archlinux.qemu.pkr.hcl") -ne 0) {
  	break
  }
  # $env:PACKER_LOG_PATH="output/custom-packerlog.txt"
  # if ((Packer-BuildAppliance -SearchFileName "*archlinux-custom-x86_64.iso" -ArgList "build -on-error=ask -only=qemu.custom archlinux.qemu.pkr.hcl") -ne 0) {
  # 	break
  # }
}