packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
    ansible = {
      source  = "github.com/hashicorp/ansible"
      version = "~> 1"
    }
  }
}


variable "yearmonthday" {
  type = string
}

variable "sound_driver" {
  type = string
}

variable "accel_graphics" {
  type = string
}

variable "cpu_cores" {
  type = number
  default = 4
}

variable "memory" {
  type = number
  default = 3072
}

variable "encryption" {
  type = bool
  default = false
}

variable "dualboot" {
  type = bool
  default = false
}

variable "headless" {
  type = bool
  default = true
}

variable "pxe" {
  type = bool
  default = false
}


source "qemu" "bootstrap" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  disk_compression         = false
  skip_compaction          = true
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  cpu_model                = "host"
  vtpm                     = true
  tpm_device_type          = "tpm-tis"
  efi_boot                 = true
  efi_firmware_code        = "/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd"
  efi_firmware_vars        = "/usr/share/OVMF/x64/OVMF_VARS.4m.fd"
  sockets                  = 1
  cores                    = var.cpu_cores
  threads                  = 1
  qemuargs                 = [["-rtc", "base=utc,clock=rt"], ["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-virtfs", "local,path=output,mount_tag=host.0,security_model=mapped,id=host.0"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/bootstrap"
  ssh_username             = "root"
  ssh_password             = "toor"
  ssh_timeout              = "10m"
  vm_name                  = "archlinux-bootstrap-${var.yearmonthday}-x86_64"
}

source "qemu" "router" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  disk_compression         = false
  skip_compaction          = true
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  cpu_model                = "host"
  vtpm                     = true
  tpm_device_type          = "tpm-tis"
  efi_boot                 = true
  efi_firmware_code        = "/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd"
  efi_firmware_vars        = "/usr/share/OVMF/x64/OVMF_VARS.4m.fd"
  sockets                  = 1
  cores                    = var.cpu_cores
  threads                  = 1
  qemuargs                 = [["-rtc", "base=utc,clock=rt"], ["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-virtfs", "local,path=output,mount_tag=host.0,security_model=mapped,id=host.0"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/router"
  ssh_username             = "root"
  ssh_password             = "toor"
  ssh_timeout              = "10m"
  vm_name                  = "archlinux-router-${var.yearmonthday}-x86_64"
}

source "qemu" "graphical" {
  shutdown_command         = "/sbin/poweroff"
  cd_files                 = ["CIDATA/*"]
  cd_label                 = "CIDATA"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  disk_compression         = false
  skip_compaction          = true
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  cpu_model                = "host"
  vtpm                     = true
  tpm_device_type          = "tpm-tis"
  efi_boot                 = true
  efi_firmware_code        = "/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd"
  efi_firmware_vars        = "/usr/share/OVMF/x64/OVMF_VARS.4m.fd"
  sockets                  = 1
  cores                    = var.cpu_cores
  threads                  = 1
  qemuargs                 = [["-rtc", "base=utc,clock=rt"], ["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-virtfs", "local,path=output,mount_tag=host.0,security_model=mapped,id=host.0"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/graphical"
  ssh_username             = "root"
  ssh_password             = "toor"
  ssh_timeout              = "10m"
  vm_name                  = "archlinux-graphical-${var.yearmonthday}-x86_64"
}


build {
  sources = ["source.qemu.bootstrap", "source.qemu.router", "source.qemu.graphical"]

  # enable shared folder in qemu
  provisioner "shell" {
    inline = [
      "mount --mkdir -t 9p -o trans=virtio,version=9p2000.L,rw host.0 /share"
    ]
  }

  # prepare ansible in archiso environment
  provisioner "shell" {
    inline = [
      "rm /usr/lib/python3*/EXTERNALLY-MANAGED",
      "python -m ensurepip 2>/dev/null",
      "python -m pip install --upgrade pip setuptools wheel",
      "python -m pip install ansible-core",
      "tee ~/.ansible.cfg <<EOF",
      "[defaults]",
      "interpreter_python=auto_silent",
      "EOF",
      "ansible-galaxy -vvvv collection install community.general"
    ]
    environment_vars = [
      "PIP_ROOT_USER_ACTION=ignore"
    ]
  }

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso.sh",
      "/root/prepare-archiso.sh --device /dev/vda ${var.encryption ? "--encryption atad" : "" } ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap"
    ]
    only   = ["qemu.bootstrap"]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap -s router -s pxeboot"
    ]
    only   = ["qemu.router"]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/install-scriptbase.sh",
      "/root/install-scriptbase.sh -w /root -s bootstrap -s graphical -s cinnamon"
    ]
    only   = ["qemu.graphical"]
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/finalize-archiso.sh",
      "/root/finalize-archiso.sh --device /dev/vda ${var.dualboot ? "--dualboot" : "" } --yes"
    ]
  }

  provisioner "shell" {
    inline = [
      "cp -a /run/archiso/bootmnt/arch/x86_64/airootfs.sfs /mnt/srv/http/arch/x86_64/pxeboot.img || true",
      "cp -a /run/archiso/bootmnt/arch/x86_64/airootfs.sfs /mnt/srv/nfs/arch/x86_64/pxeboot.img || true",
      "cp -a /run/archiso/bootmnt/arch/x86_64/airootfs.sfs /mnt/srv/nbd/arch/x86_64/pxeboot.img || true",
      "cp -a /run/archiso/bootmnt/arch/x86_64/airootfs.sfs /mnt/srv/cifs/arch/x86_64/pxeboot.img || true",
      "cp -a /run/archiso/bootmnt/boot/syslinux/splash.png /mnt/srv/tftp/bios/ || true",
      "cp -a /run/archiso/bootmnt/boot/syslinux/splash.png /mnt/srv/tftp/efi32/ || true",
      "cp -a /run/archiso/bootmnt/boot/syslinux/splash.png /mnt/srv/tftp/efi64/ || true",
      "chown -R root:root /mnt/srv/tftp /mnt/srv/http /mnt/srv/nfs /mnt/srv/cifs",
      "find /mnt/srv/tftp /mnt/srv/http /mnt/srv/nfs /mnt/srv/cifs -type d -exec chmod 755 {} \\;",
      "find /mnt/srv/tftp /mnt/srv/http /mnt/srv/nfs /mnt/srv/cifs -type f -exec chmod 644 {} \\;"
    ]
    only   = ["qemu.router"]
  }

  provisioner "shell-local" {
    inline  = [ <<EOS
tee output/bootstrap/archlinux-bootstrap-${var.yearmonthday}-x86_64.run.sh <<EOF
#!/usr/bin/env bash
mkdir -p "/tmp/swtpm.0" "share"
/usr/bin/swtpm socket --tpm2 --tpmstate dir="/tmp/swtpm.0" --ctrl type=unixio,path="/tmp/swtpm.0/vtpm.sock" &
/usr/bin/qemu-system-x86_64 \\
  -name archlinux-bootstrap-${var.yearmonthday}-x86_64 \\
  -machine type=q35,accel=kvm \\
  -vga virtio \\
  -cpu host \\
  -drive file=archlinux-bootstrap-${var.yearmonthday}-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -device tpm-tis,tpmdev=tpm0 -tpmdev emulator,id=tpm0,chardev=vtpm -chardev socket,id=vtpm,path=/tmp/swtpm.0/vtpm.sock \\
  -drive file=/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd,if=pflash,unit=0,format=raw,readonly=on \\
  -drive file=efivars.fd,if=pflash,unit=1,format=raw \\
  -smp ${var.cpu_cores},sockets=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user.0 -device virtio-net,netdev=user.0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -virtfs local,path=share,mount_tag=host.0,security_model=mapped,id=host.0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=host
EOF
chmod +x output/bootstrap/archlinux-bootstrap-${var.yearmonthday}-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.bootstrap"]
  }

  provisioner "shell-local" {
    inline  = [ <<EOS
tee output/router/archlinux-router-${var.yearmonthday}-x86_64.run.sh <<EOF
#!/usr/bin/env bash
mkdir -p "/tmp/swtpm.0" "share"
/usr/bin/swtpm socket --tpm2 --tpmstate dir="/tmp/swtpm.0" --ctrl type=unixio,path="/tmp/swtpm.0/vtpm.sock" &
/usr/bin/qemu-system-x86_64 \\
  -name archlinux-router-${var.yearmonthday}-x86_64 \\
  -machine type=q35,accel=kvm \\
  -vga virtio \\
  -cpu host \\
  -drive file=archlinux-router-${var.yearmonthday}-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -device tpm-tis,tpmdev=tpm0 -tpmdev emulator,id=tpm0,chardev=vtpm -chardev socket,id=vtpm,path=/tmp/swtpm.0/vtpm.sock \\
  -drive file=/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd,if=pflash,unit=0,format=raw,readonly=on \\
  -drive file=efivars.fd,if=pflash,unit=1,format=raw \\
  -smp ${var.cpu_cores},sockets=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user.0 -device virtio-net,netdev=user.0 \\
  -netdev socket,id=user.1,listen=:34689 -device virtio-net,netdev=user.1 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -virtfs local,path=share,mount_tag=host.0,security_model=mapped,id=host.0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=host
EOF
chmod +x output/router/archlinux-router-${var.yearmonthday}-x86_64.run.sh
cp /usr/share/OVMF/x64/OVMF_VARS.4m.fd output/router/efivars.1.fd
tee output/router/archlinux-pxe-${var.yearmonthday}-x86_64.run.sh <<EOF
#!/usr/bin/env bash
mkdir -p "/tmp/swtpm.1" "share"
/usr/bin/swtpm socket --tpm2 --tpmstate dir="/tmp/swtpm.1" --ctrl type=unixio,path="/tmp/swtpm.1/vtpm.sock" &
/usr/bin/qemu-system-x86_64 \\
  -name archlinux-pxe-${var.yearmonthday}-x86_64 \\
  -machine type=q35,accel=kvm \\
  -vga virtio \\
  -cpu host \\
  -device tpm-tis,tpmdev=tpm0 -tpmdev emulator,id=tpm0,chardev=vtpm -chardev socket,id=vtpm,path=/tmp/swtpm.1/vtpm.sock \\
  -drive file=/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd,if=pflash,unit=0,format=raw,readonly=on \\
  -drive file=efivars.1.fd,if=pflash,unit=1,format=raw \\
  -smp ${var.cpu_cores},sockets=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev socket,id=user.0,connect=:34689 -device virtio-net,netdev=user.0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -virtfs local,path=share,mount_tag=host.0,security_model=mapped,id=host.0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=host
EOF
chmod +x output/router/archlinux-pxe-${var.yearmonthday}-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.router"]
  }

  provisioner "shell-local" {
    inline  = [ <<EOS
tee output/graphical/archlinux-graphical-${var.yearmonthday}-x86_64.run.sh <<EOF
#!/usr/bin/env bash
mkdir -p "/tmp/swtpm.0" "share"
/usr/bin/swtpm socket --tpm2 --tpmstate dir="/tmp/swtpm.0" --ctrl type=unixio,path="/tmp/swtpm.0/vtpm.sock" &
/usr/bin/qemu-system-x86_64 \\
  -name archlinux-graphical-${var.yearmonthday}-x86_64 \\
  -machine type=q35,accel=kvm \\
  -vga virtio \\
  -cpu host \\
  -drive file=archlinux-graphical-${var.yearmonthday}-x86_64,if=virtio,cache=writeback,discard=unmap,detect-zeroes=unmap,format=qcow2 \\
  -device tpm-tis,tpmdev=tpm0 -tpmdev emulator,id=tpm0,chardev=vtpm -chardev socket,id=vtpm,path=/tmp/swtpm.0/vtpm.sock \\
  -drive file=/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd,if=pflash,unit=0,format=raw,readonly=on \\
  -drive file=efivars.fd,if=pflash,unit=1,format=raw \\
  -smp ${var.cpu_cores},sockets=1,cores=${var.cpu_cores},maxcpus=${var.cpu_cores} -m ${var.memory}M \\
  -netdev user,id=user.0 -device virtio-net,netdev=user.0 \\
  -audio driver=pa,model=hda,id=snd0 -device hda-output,audiodev=snd0 \\
  -virtfs local,path=share,mount_tag=host.0,security_model=mapped,id=host.0 \\
  -usbdevice mouse -usbdevice keyboard \\
  -rtc base=utc,clock=host
EOF
chmod +x output/graphical/archlinux-graphical-${var.yearmonthday}-x86_64.run.sh
EOS
    ]
    only_on = ["linux"]
    only    = ["qemu.graphical"]
  }

  provisioner "shell" {
    inline = var.pxe ? [
      "chmod +x /root/finalize-pxe-squash.sh", 
      "/root/finalize-pxe-squash.sh"
    ] : [
      "echo 'no pxe option chosen'"
    ]
  }
}


source "qemu" "custom" {
  shutdown_command         = "/sbin/poweroff"
  boot_command             = ["<enter><wait1m>", "echo -e 'toor\\ntoor' | (passwd)<enter><wait2>", "mkdir /share<enter><wait2>", "mount -t 9p -o trans=virtio,version=9p2000.L,rw host.0 /share<enter><wait2>"]
  boot_wait                = "10s"
  disk_size                = 524288
  memory                   = var.memory
  format                   = "qcow2"
  accelerator              = "kvm"
  disk_discard             = "unmap"
  disk_detect_zeroes       = "unmap"
  disk_interface           = "virtio"
  disk_compression         = false
  skip_compaction          = true
  net_device               = "virtio-net"
  vga                      = "virtio"
  machine_type             = "q35"
  cpu_model                = "host"
  vtpm                     = true
  tpm_device_type          = "tpm-tis"
  efi_boot                 = true
  efi_firmware_code        = "/usr/share/OVMF/x64/OVMF_CODE.secboot.4m.fd"
  efi_firmware_vars        = "/usr/share/OVMF/x64/OVMF_VARS.4m.fd"
  sockets                  = 1
  cores                    = var.cpu_cores
  threads                  = 1
  qemuargs                 = [["-rtc", "base=utc,clock=rt"], ["-usbdevice", "mouse"], ["-usbdevice", "keyboard"], ["-virtfs", "local,path=output,mount_tag=host.0,security_model=mapped,id=host.0"]]
  headless                 = var.headless
  iso_checksum             = "none"
  iso_url                  = "archlinux-${var.yearmonthday}-x86_64.iso"
  output_directory         = "output/custom"
  ssh_username             = "root"
  ssh_password             = "toor"
  ssh_timeout              = "10m"
  vm_name                  = "archlinux-bootstrap-${var.yearmonthday}-x86_64"
}


build {
  sources = ["source.qemu.custom"]

  # trailing slash: content of archlinux-scriptbase is copied directly into the /root folder
  provisioner "file" {
    source = "archlinux-scriptbase/"
    destination = "/root"
  }

  provisioner "shell" {
    inline = [
      "chmod +x /root/prepare-archiso-custom.sh",
      "/root/prepare-archiso-custom.sh --device /dev/vda --yes"
    ]
  }
}
